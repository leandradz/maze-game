/* MAPA */
const map = [
  "WWWWWWWWWWWWWWWWWWWWW",
  "W   W     W     W W W",
  "W W W WWW WWWWW W W W",
  "W W W   W     W W   W",
  "W WWWWWWW W WWW W W W",
  "W         W     W W W",
  "W WWW WWW W WWWWW W W",
  "W W   W   W W     W W",
  "W WWWWW W W W WWW W F",
  "S     W W W W W W WWW",
  "WWWWW W W W W W W W W",
  "W     W W W   W W W W",
  "W WWWWWWW WWWWW W W W",
  "W       W       W   W",
  "WWWWWWWWWWWWWWWWWWWWW",
];
const main = document.getElementById("main")
const container = document.getElementById('container')
const divStart = document.getElementById('start')
const start = document.getElementById('btnStart')
const muteBtn = document.getElementById('mute')

function pegarCelula(linha) {
  for (let i = 0; i < map.length; i++) {
    for (let j = 0; j < map[i].length; j++) {

      let divCelula = document.createElement('div')
      divCelula.innerText = map[i][j]
      
      /* ESTILOS E DECORAÇÕES */
      if (divCelula.innerText === 'S') {
        divCelula.style.backgroundImage = "url('./img/asfalto.png')"
        divCelula.id = 'celula'
        //Personagem
        let divPersonagem = document.createElement('div')
        divPersonagem.id = 'personagem'
        divPersonagem.style.backgroundImage = "url('./img/personagem-direita.png')"
        divCelula.appendChild(divPersonagem)

      } else if (divCelula.innerText === 'F') {
        divCelula.id = 'chegada'
        divCelula.style.backgroundImage = "url('./img/mototaxista.png')"

      } else {
        divCelula.id = 'celula'

        if (divCelula.innerText === ' ') {
          divCelula.style.backgroundImage = "url('./img/asfalto.png')"

        } else {
          divCelula.style.backgroundImage = "url('./img/parede.png')"
          divCelula.style.position.fixed
        }
      }
      let linhaAtual = document.getElementById(`linha${i}`)
      linhaAtual.appendChild(divCelula)
    }
  }
}

function pegarLinha() {
  for (let i = 0; i < map.length; i++) {
    let divLinha = document.createElement('div')
    divLinha.id = `linha${i}`
    divLinha.className = 'linha'
    container.appendChild(divLinha)
  }
}

  /*Vitória*/
  function vitoria() {
    muteBtn.style.display = 'none'
    container.style.display = "none"
    let som = document.getElementById('som')
    som.play()
    let vitoria = document.createElement("div")
    main.appendChild(vitoria)
    vitoria.id = "vitoria"
    vitoria.style.lineHeight = 5
    vitoria.innerText = 'Obrigada!! Você fez com que a Letícia chegue mais rápido ao seu destino!'

    let restart = document.createElement('button')
    restart.id = 'restart'
    restart.innerText = 'Restart'
    restart.style.display = "block"
    vitoria.appendChild(restart)
    restart.addEventListener("click", function (event) {
      return location.reload()
    });
  }

start.addEventListener("click", function (event) {
  container.style.display = 'block'
  divStart.style.display = 'none'
  muteBtn.style.display = 'block'

  pegarLinha()
  pegarCelula()

  /* BOTÃO MUDO */
  let statusMute = document.getElementById('somMove').muted = false
  muteBtn.addEventListener("click", function (event) {
  if (statusMute == false) {
    statusMute = document.getElementById('somMove').muted = true
    muteBtn.style.backgroundImage = "url('./img/muteBtn.png')"
  } else {
    statusMute = document.getElementById('somMove').muted = false
    muteBtn.style.backgroundImage = "url('./img/somBtn.png')"
  }
})

  /* KeyboardEvent */
  const personagem  = document.getElementById('personagem')

  'use strict';

  let boxTop = 310
  let boxLeft = 0
  let linhaInicio = 9
  let celulaInicio = 0

  document.addEventListener('keydown', (event) => {
    const keyName = event.key;

    if (boxTop >= 0 && boxTop <= 630 && boxLeft >= 0 && boxLeft <= 930) {
      
      if (keyName === 'ArrowDown') {
        if (boxTop < 620 && map[linhaInicio + 1][celulaInicio] === ' ') {
          personagem.style.top = (boxTop += 35) + "px";
          document.getElementById('somMove').play()
          linhaInicio++
        }
      } else if (keyName === 'ArrowUp') {
        if (boxTop > 10 && linhaInicio >= 0 && map[linhaInicio - 1][celulaInicio] === ' ') {
          personagem.style.top = (boxTop -= 35) + "px";
          document.getElementById('somMove').play()
          linhaInicio--
        }
      } else if (keyName === 'ArrowRight') {
        if (linhaInicio === 8 && celulaInicio === 19) {
          vitoria()

        } else if (boxLeft < 920 && map[linhaInicio][celulaInicio + 1] === ' ') {
          personagem.style.left = (boxLeft += 35) + "px";
          document.getElementById('somMove').play()
          personagem.style.backgroundImage = "url('./img/personagem-direita.png')"
          celulaInicio++
        }
      } else if (keyName === 'ArrowLeft') {
        if (boxLeft > 10 && celulaInicio >= 0 && map[linhaInicio][celulaInicio - 1] === ' ') {
          personagem.style.left = (boxLeft -= 35) + "px";
          document.getElementById('somMove').play()
          personagem.style.backgroundImage = "url('./img/personagem-esquerda.png')"
          celulaInicio--
        }
      }
    }
  })
})
